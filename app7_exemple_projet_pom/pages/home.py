from playwright.sync_api import Page


class HomePage:

    dico_locator = {
        "url": "https://www.randstaddigital.fr/fr/",
        "accepter_les_cookies": "//button[@id='onetrust-accept-btn-handler']"
        }

    def __init__(self, page: Page):
        self.page = page
    
    def acceder_application(self):
        self.page.goto(self.dico_locator["url"])

    def cliquer_sur_accepter_les_cookies(self):
        self.page.locator(self.dico_locator["accepter_les_cookies"]).click()
