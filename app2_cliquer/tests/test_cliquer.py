from pages.home import HomePage
from playwright.sync_api import expect, Page

def test_cliquer(page: Page, result_page: HomePage):

    # EN TANT QUE : Utilisateur


    # JE VEUX : Naviguer sur l'application web
    result_page.acceder_application()

    # POUR : Verifier que l'on puisse naviguer dessus
    result_page.cliquer_sur_get_started()
    locator = page.locator('//h1')
    locator.highlight()
    expect(locator).to_be_visible()
    page.wait_for_timeout(5000)


