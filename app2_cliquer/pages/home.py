from playwright.sync_api import Page


class HomePage:

    dico_locator = {
        "url": "https://playwright.dev/python/",
        "get_started": "//div[@class='buttons_pzbO']//a[@href='/python/docs/intro']"
        }

    def __init__(self, page: Page):
        self.page = page
    
    def acceder_application(self):
        self.page.goto(self.dico_locator["url"])

    def cliquer_sur_get_started(self):
        self.page.locator(self.dico_locator["get_started"]).click()

