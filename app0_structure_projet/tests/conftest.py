
import pytest

from pages.home import HomePage
from playwright.sync_api import Page


# ------------------------------------------------------------
# Declarer les fixtures
# ------------------------------------------------------------

@pytest.fixture
def result_page(page: Page):
    return HomePage(page)
