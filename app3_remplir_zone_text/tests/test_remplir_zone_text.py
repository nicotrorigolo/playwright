from pages.home import HomePage
from pages.bandeau import BandeauPage
from playwright.sync_api import expect, Page

def test_remplir_zone_text(page: Page, result_page: HomePage, bandeau_page: BandeauPage):

    # EN TANT QUE : Utilisateur


    # JE VEUX : Faire une recherche sur l'application web
    result_page.acceder_application()

    # POUR : Verifier que l'on puisse faire une recherche
    bandeau_page.cliquer_sur_search()
    bandeau_page.remplir_zone_texte_search()
    locator = page.locator('//li[@id="docsearch-item-0"]')
    locator.highlight()
    expect(locator).to_be_visible()
    page.wait_for_timeout(5000)
