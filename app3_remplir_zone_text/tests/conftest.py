
import pytest

from pages.home import HomePage
from pages.bandeau import BandeauPage
from playwright.sync_api import Page


# ------------------------------------------------------------
# Declarer les fixtures
# ------------------------------------------------------------

@pytest.fixture
def result_page(page: Page):
    return HomePage(page)

@pytest.fixture
def bandeau_page(page: Page):
    return BandeauPage(page)
