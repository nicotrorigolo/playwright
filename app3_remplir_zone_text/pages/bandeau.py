from playwright.sync_api import Page


class BandeauPage:

    dico_locator = {
        "bouton_search": "//button[@aria-label='Search']",
        "zone_texte_search": "//input[@id='docsearch-input']"
        }

    dico_input = {
        "zone_texte_search": "playwright"
        }

    def __init__(self, page: Page):
        self.page = page

    def cliquer_sur_search(self):
        self.page.locator(self.dico_locator["bouton_search"]).click()

    def remplir_zone_texte_search(self):
        self.page.locator(self.dico_locator["zone_texte_search"]).fill(self.dico_input["zone_texte_search"])
