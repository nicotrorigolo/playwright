from playwright.sync_api import Page


class HomePage:

    URL = "https://playwright.dev/python/"

    def __init__(self, page: Page):
        self.page = page
    
    def acceder_application(self):
        self.page.goto(self.URL)
