from pages.home import HomePage
from playwright.sync_api import expect, Page

def test_ouverture_application(page: Page, result_page: HomePage):

    # EN TANT QUE : Utilisateur


    # JE VEUX : Ouvrir l'application web
    result_page.acceder_application()

    # POUR : Verifier que l'application soit en ligne
    locator = page.locator('//b[contains(text(),"Playwright for Python")]')
    locator.highlight()
    page.wait_for_timeout(5000)
    expect(locator).to_be_visible()


