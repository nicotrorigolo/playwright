from pages.home import HomePage
from pages.bandeau import BandeauPage
from pages.toute_nos_offres import Toutes_nos_offrePage


def test_remplir_zone_text(homepage: HomePage, bandeaupage: BandeauPage, toutes_nos_offrePage: Toutes_nos_offrePage):

    # EN TANT QUE : Utilisateur
    # homepage.browser.new_context().set_default_timeout(1000)
    # aaa = browser.new_context().set_default_timeout(1000)

    # page = context.new_page()

    # JE VEUX : Faire une recherche sur l'application web
    homepage.acceder_application()
    homepage.cliquer_sur_accepter_les_cookies()
    bandeaupage.cliquer_sur_carriere()
    bandeaupage.cliquer_sur_toutes_nos_offres()
    toutes_nos_offrePage.renseigner_recherche()
    toutes_nos_offrePage.cliquer_sur_rechercher()
    # POUR : Verifier que l'on puisse faire une recherche
    # bandeau_page.cliquer_sur_search()
    # bandeau_page.remplir_zone_texte_search()
    # locator = page.locator('//li[@id="docsearch-item-0"]')
    # locator.highlight()
    # expect(locator).to_be_visible()
    # page.wait_for_timeout(5000)
    # homepage.cliquer_sur_accepter_les_cookies()

# def test_se_connecter_au_site(page: Page, homepage: HomePage, bandeaupage: BandeauPage, toutes_nos_offrePage: Toutes_nos_offrePage):

#     # EN TANT QUE : Utilisateur


#     # JE VEUX : Faire une recherche sur l'application web
#     homepage.acceder_application()
#     homepage.cliquer_sur_accepter_les_cookies()
#     # toutes_nos_offrePage.cliquer_sur_rechercher()
#     # POUR : Verifier que l'on puisse se connecter au site

