
import pytest

from pages.home import HomePage
from pages.bandeau import BandeauPage
from pages.toute_nos_offres import Toutes_nos_offrePage
from playwright.sync_api import Page

# ------------------------------------------------------------
# Declarer les fixtures
# ------------------------------------------------------------
  
@pytest.fixture
def homepage(page: Page):
    return HomePage(page)

@pytest.fixture
def bandeaupage(page: Page):
    return BandeauPage(page)

@pytest.fixture
def toutes_nos_offrePage(page: Page):
    return Toutes_nos_offrePage(page)

