from playwright.sync_api import Page

class Toutes_nos_offrePage:

    dico_locator = {
        "rechercher": "//input[@id='searchField']/../../..//span[contains(text(),'offre')]/..",
        "recherche": "//input[@id='searchField']"
        }

    dico_input = {
        "recherche": "Boulanger"
        }

    def __init__(self, page: Page):
        self.page = page

    def cliquer_sur_rechercher(self):
        self.page.locator(self.dico_locator["rechercher"]).click()

    def renseigner_recherche(self):
        self.page.locator(self.dico_locator["recherche"]).fill(self.dico_input["recherche"])
