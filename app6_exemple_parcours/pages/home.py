import os
import builtins
from playwright.sync_api import Page, TimeoutError as playwright_Timeout



class HomePage:

    dico_locator = {
        "url": "https://www.randstaddigital.fr/fr/",
        "accepter_les_cookies": "//button[@id='onetrust-accept-btn-handler']"
        }

    def __init__(self, page: Page):
        self.page = page
    
    def acceder_application(self):
        try:
            self.page.set_default_timeout(float(os.environ["TIMEOUT_GLOBAL"]))
            self.page.goto(self.dico_locator["url"])
        except playwright_Timeout as e:
            print("OKffffffffffffffffffffffffffffffffffffffffffffffffddddddddddddddddddd", "===============")


    def cliquer_sur_accepter_les_cookies(self):
        self.page.locator(self.dico_locator["accepter_les_cookies"]).click()
