from playwright.sync_api import Page


class BandeauPage:

    dico_locator = {
        "carriere": "(//a[@href='/fr/carrieres/'])[1]",
        "toutes_nos_offres": "(//a[@href='/fr/carrieres/toutes-nos-offres/'])[1]"
        }

    def __init__(self, page: Page):
        self.page = page

    def cliquer_sur_carriere(self):
        self.page.locator(self.dico_locator["carriere"]).click()

    def cliquer_sur_toutes_nos_offres(self):
        self.page.locator(self.dico_locator["toutes_nos_offres"]).click()

    def remplir_zone_texte_search(self):
        self.page.locator(self.dico_locator["zone_texte_search"]).fill(self.dico_input["zone_texte_search"])
