from playwright.sync_api import sync_playwright

def test_api(playwright: sync_playwright):
    context = playwright.request.new_context(base_url="https://pokeapi.co")
    response = context.get(url="/api/v2/pokemon/pikachu")
    print(response)
    print(response.json()["name"])
    assert response.status == 200